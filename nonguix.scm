(list (channel (name `guix) (url "https://git.savannah.gnu.org/git/guix.git")
               (branch :master)
               (commit :ba19307af07945fe0e519d3b1a12d3642a73c247)
               (introduction (make-channel-introduction :9edb3f66fd807b096b48283debdcddccfea34bad
                                                        (openpgp-fingerprint "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel (name `nonguix) (url "https://gitlab.com/nonguix/nonguix")
               (branch :master)
               (commit :39e4b41e5f7277b8d58084cd7aff8edde71f6572)
               (introduction (make-channel-introduction :897c1a470da759236cc11798f4e0a5f7d4d59fbc
                                                        (openpgp-fingerprint "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5")))))

