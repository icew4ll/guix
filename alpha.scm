(define-module (nongnu system install)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mtools)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages)
  #:use-module (gnu services base)
  #:use-module (gnu services dbus)
  #:use-module (gnu services desktop)
  #:use-module (gnu services sddm)
  #:use-module (gnu services security-token)
  #:use-module (gnu services)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system install)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system pam)
  #:use-module (gnu system shadow)
  #:use-module (gnu system)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (nongnu packages linux)
  #:use-module (srfi srfi-1)
  #:export (installation-os-nonfree))

(define installation-os-nonfree
  (operating-system
   (inherit installation-os)

   (kernel linux)
   (firmware (list linux-firmware))

   (keyboard-layout
    (keyboard-layout "us"))

   ;; Add the 'net.ifnames' argument to prevent network interfaces
   ;; from having really long names.  This can cause an issue with
   ;; wpa_supplicant when you try to connect to a wifi network.
   (kernel-arguments '("quiet" "modprobe.blacklist=radeon" "net.ifnames=0"))

   (services
    (cons*
     (service pcscd-service-type)
     (polkit-service)
     (service mingetty-service-type (mingetty-configuration
                                     (tty "tty8")))
     (elogind-service)
     ;; Include the channel file so that it can be used during installation
     (simple-service 'channel-file etc-service-type
                     (list `("channels.scm" ,(local-file "channels.scm"))))
     (operating-system-user-services installation-os)))

   (setuid-programs (list (file-append shadow "/bin/passwd")
                          (file-append sudo "/bin/sudo")))

   ;; Add some extra packages useful for the installation process
   (packages (append
              (map specification->package+output
                   '("htop"
                     "font-iosevka" "font-dejavu" "font-gnu-unifont"
                     "emacs" "emacs-guix" "emacs-use-package"
                     "emacs-magit"
                     "alacritty"
                     "ungoogled-chromium"
                     "light"
                     "git" "make" "iwd"
                     "grub"
                     "glibc" "nss-certs"))
              %base-packages-disk-utilities
              %base-packages))))

installation-os-nonfree
